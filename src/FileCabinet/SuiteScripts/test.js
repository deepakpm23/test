function fieldChangeEvent(type, name, linenum) {
var context=nlapiGetContext();
var currentUser=context.getUser();
if(currentUser=="200580")
{
	//currentUser=3165;
}

if (name == 'custpage_customer') {
		nlapiRemoveSelectOption('custpage_project', null);
		nlapiInsertSelectOption('custpage_project','All','All',true);
		var request_customer_id=nlapiGetFieldValue('custpage_customer');
  		var request_project_status=nlapiGetFieldValue('custpage_project_status');
		if(request_customer_id && request_customer_id!="All"){
             //   var projectList=getRelatedProjects(currentUser,request_customer_id,request_project_status);
            var suiteletUrl = nlapiResolveURL('SUITELET', 'customscript_sut_ado_weekly_ts_populate_', 'customdeploy_sut_ado_weekly_ts_populate_'); //Get the Suitelet URL
            var a = new Array();
            a['User-Agent-x'] = 'SuiteScript-Call';

var response = nlapiRequestURL(suiteletUrl+ '&i_customer=' +request_customer_id+'&status=' +request_project_status,null,null);
          var res = response.getBody();	
          var projectList = JSON.parse(res);
				if (projectList) {
					//nlapiInsertSelectOption('custpage_project','All','All',true);
					for (var i = 0; i < projectList.length; i++) {
						nlapiInsertSelectOption('custpage_project',projectList[i]['columns']['entityid'],
								projectList[i]['columns']['entityid'] + " "
										+ projectList[i]['columns']['altname'],false);
					}
					
				}
		}
	}
        if(name=="custpage_project_status")
          {
              nlapiRemoveSelectOption('custpage_project', null);
              nlapiInsertSelectOption('custpage_project','All','All',true);
                var request_customer_id=nlapiGetFieldValue('custpage_customer');
                var request_project_status=nlapiGetFieldValue('custpage_project_status');
             //   var projectList=getRelatedProjects(currentUser,request_customer_id,request_project_status);
            var suiteletUrl = nlapiResolveURL('SUITELET', 'customscript_sut_ado_weekly_ts_populate_', 'customdeploy_sut_ado_weekly_ts_populate_'); //Get the Suitelet URL
            var a = new Array();
            a['User-Agent-x'] = 'SuiteScript-Call';

var response = nlapiRequestURL(suiteletUrl+ '&i_customer=' +request_customer_id+'&status=' +request_project_status,null,null);
          var res = response.getBody();	
          var projectList = JSON.parse(res);
				if (projectList) {
					//nlapiInsertSelectOption('custpage_project','All','All',true);
					for (var i = 0; i < projectList.length; i++) {
						//nlapiInsertSelectOption('custpage_project',projectList[i].getId(),
							nlapiInsertSelectOption('custpage_project',projectList[i]['columns']['entityid'],
								projectList[i]['columns']['entityid'] + " "
										+ projectList[i]['columns']['altname'],false);
					}
					
				}
            else
              {
                nlapiRemoveSelectOption('custpage_project', null);
              }
            
          }
}


function getRelatedProjects(currentUser,request_customer_id,status) {
	var filters = [
	        ["customer.custentity_account_delivery_owner","anyof",currentUser]
	        //,'and',
	        //[ [ 'status', 'anyof', '2' ], 'or', [ 'status', 'anyof', '4' ] ] 
	        ];
			if(request_customer_id)
			{
				filters.push('and');
				filters.push(["customer","anyof",request_customer_id]);
			}
  
         if(status)
			{
				filters.push('and');
				filters.push(["status","anyof",status]);
			}

	var columns = [ new nlobjSearchColumn("entityid"),
	        new nlobjSearchColumn("altname") ];

	var search = nlapiSearchRecord('job', null, filters, columns);
	return search;
}

function on_save()
{
  var suiteletUrl = nlapiResolveURL('SUITELET', 'customscript_sut_ado_project_profitabili', 'customdeploy_sut_ado_project_profitabili', false);

      var param = '&customer=' + nlapiGetFieldValue('custpage_customer');

       var finalUrl = suiteletUrl + param;

       var newWindowParams = "width=750, height=400,resizeable = 1, scrollbars = 1," +

       "toolbar = 0, location = 0, directories = 0, status = 0, menubar = 0, copyhistory = 0";

       var setWindow = "window.open('" + finalUrl + "',_self,'" + newWindowParams + "')";

}

function goBack() {
	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_ado_pp_screen',
	        'customdeploy1');
}

function refreshProfibility() {
	var customer = nlapiGetFieldValue('custpage_customer');
	//var project = nlapiGetFieldValues('custpage_project');
	var project = nlapiGetFieldValues('custpage_project');
  var project_array=project.toString();
  project=project_array.split(',');
	var status = nlapiGetFieldValue('custpage_project_status');
  var startdate = nlapiGetFieldValue('custpage_fromdate');
  var enddate = nlapiGetFieldValue('custpage_todate');
  var efc=nlapiGetFieldValue('custpage_efc');
 
/*
 var options = nlapiGetField('custpage_project').getSelectOptions();
  nlapiLogExecution('debug','options',options);
  */
  nlapiLogExecution('debug','project.length',project.length);
  var projectlist='';
  for(var i=0;project.length>i;i++)
    {
      projectlist+="&project="+project[i];
    }
  nlapiLogExecution('debug','projectlist',projectlist);
  
	window.location = nlapiResolveURL('SUITELET',
	        'customscript_sut_ado_project_profitabili',
	        'customdeploy_sut_ado_project_profitabili')
	         + "&customer="
	         + customer
	        +projectlist
	        + "&from="
	        + startdate + "&to=" + enddate + "&status=" + status+ "&efc=" + efc;
}
